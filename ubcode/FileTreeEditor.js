/************************
* Tree controller adds File control bar (UI) and delete-key support.
************************/
define([
    "dojo/on", 
    "dojo/dom", 
    "dojo/keys", 
    "dojo/aspect", 
    "dijit/registry", 
    "dojo/_base/lang", 
	"dojo/_base/array", 
	"dojo/_base/declare", 
	"dojo/dom-construct", 
	"dijit/_WidgetBase", 
	"dijit/form/Form", 
	"dijit/form/Button", 
	"dijit/Dialog", 
    "dojo/json", 
 	"dijit/_TemplatedMixin", 
 	"dijit/_WidgetsInTemplateMixin", 
    "ubcode/FileNodeForm", 
 	// Replace class-name with name of class.  
 	"dojo/text!./templates/FileTreeEditor.html"], 
 	function(on, dom, keys, aspect, registry, lang, array, declare, 
        construct, WidgetBase, Form, Button, Dialog, json, Templated, 
        WidgetsInTemplateMixin, FileNodeForm, template) {
 		return declare("ubcode.FileTreeEditor", 
            [WidgetBase, Templated, WidgetsInTemplateMixin], {
 			tree: null, 
			widgetsInTemplate: true, 
			templateString: template, 
			startup: function() {
                var self = this; 
                console.log(this.declaredClass, "startup()");
                // Setup enter-key submit support...
                on(dom.byId("body"), "keydown", function(event) {
                    var charCode = event.charCode || event.keyCode; 
                    if (charCode == keys.ENTER && self.editor) 
                        self.editor.onSave(); 
                }); 
                // Enable/disable add-new file-node based on item selection.
                aspect.after(this.tree, "onClick", function(item) {
                    var celibate = !self.tree.model.mayHaveChildren(item); 
                    self.newButton.set("disabled", celibate); 
                    self.editButton.set(
                        "disabled", this.tree.model.isRoot(item)); 
                }, true); 
                return this.inherited(arguments); 
			}, 
			getSelectedItem: function() {
                var selected = this.tree.selectedItem; 
                return selected || this.tree.model.getRoot(); 
            }, 
            onNew: function(event) {
                var parent = this.getSelectedItem(); 
                if (!this.tree.model.mayHaveChildren(parent))
                    throw new Error("Invalid parent."); 
                var newItem = this.createNew(parent);
                this.openEdit(newItem); 
            }, 
            createNew: function(parent, values) {
                values = values || {}; 
                values.type = values.type || "folder"; 
                values.name = values.name || "Burro"; 
                var counter = 0; 
                var name = values.name; 
                while (!this.tree.model.canHaveChild(parent, name))
                    name = values.name + ": " + counter++; 
                values.name = name; 
                return this.tree.model.makeItem(values, parent); 
            }, 
            onEdit: function(event) {
                var selected = this.tree.selectedItem; 
                if (selected)
                    this.openEdit(selected); 
            }, 
            openEdit: function(item) {
                var self = this; 
                var model = this.tree.model; 
                // Get Model's item instead of Tree's cached one. 
                item = model.asItem(item); 
                // In production code a single editor and 
                // dialog instance should be used.  The editor's 'item' 
                // attr would be changed with each edit, and the dialog 
                // hidden instead of destroyed.
                var dialog = new Dialog(); 
                this.editor = new FileNodeForm({
                    value: item, 
                    save: function(changes) {
                        dialog.hide(); 
                        model.update(item, changes); 
                        dialog.destroy(); 
                        delete(this.editor); 
                    }
                }); 
                this.editor.typeSelector.set(
                    "disabled", model.hasChildren(item)); 
                dialog.addChild(this.editor); 
                dialog.show(); 
            },
            onExport: function(event, item) {
                item = item || this.getSelectedItem(); 
                var content = this.tree.model.getContent(item); 
                var src = json.stringify(content, "\n", "    "); 
                var dialog = new Dialog({
                    content: "<pre>" + src + "</pre>"
                }); 
                dialog.show(); 
                console.log("onExport()", item); 
            }
		}); // End of declare() return.
	} // End of callback function. 
);
