/************************
* Add keyboard controll (BACKSPACE key) to Tree widget. 
* 
* Note: must capture BACKSPACE on <body> to block default browser 
* back-button binding.  Uses selected tree nodes from tree widget 
* to determine if/what gets deleted.  Supports multiple delete.  
* Actual actions applied to data model being observed by tree, 
* and possibly other, widgets. 
************************/
define([
    "dojo/on", 
    "dojo/dom", 
    "dojo/keys", 
    "dojo/dom-attr", 
    "dojo/Stateful", 
    "dijit/registry", 
    "dojo/_base/lang", 
	"dojo/_base/array", 
	"dojo/_base/declare"], 
 	function(on, dom, keys, attr, Stateful, registry, lang, array, declare) {
 		return declare("ubcode.KeyboardTreeController", [Stateful], {
 			tree: null, 
            constructor: function() {
                console.log(this.declaredClass, "constructor()"); 
            }, 
            _treeSetter: function(tree) {
                var self = this; 
                this.tree = tree; 
                console.log(this.declaredClass, "set('tree', tree)");
                // Setup delete-key support...
                // Delete-key capture belongs here because this 
                // widget makes tree editable, and delete could/can 
                // just have easily been implemented as a button.
                on(document.body, "keydown", function(event) {
                    var charCode = event.charCode || event.keyCode; 
                    if (charCode != keys.BACKSPACE)
                        return; 
                    if (event.target.type && event.target.type == "text")
                        return;
                    event.preventDefault();
                    if (!self.tree.selectedNodes.length)
                        return;
                    var widget = registry.getEnclosingWidget(event.target); 
                    if (self.tree.selectedNodes.indexOf(widget) == -1)
                        return; 
                    var ids = array.map(
                        self.tree.selectedItems, "return item.id;"); 
                    array.forEach(ids, 
                        self.tree.model.store.remove, self.tree.model.store); 
                }); 
			}
		});
	}
);
