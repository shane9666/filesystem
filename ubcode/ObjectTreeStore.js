/***********************
* Hierarchical object-store.
*
* Extends standard Dojo Object Store with hierarchical parent/child 
* functionality, including put() with optional "parent" option.
***********************/
define([
    "dojo/_base/lang", 
    "dojo/aspect", 
    "dojo/_base/declare",  
	"dojo/_base/array", 
	"dojo/store/Memory", 
	"dojo/store/Observable"], 
	function(lang, aspect, declare, array, MemoryStore, Observable) {
        return declare("ubcode.ObjectTreeStore", [MemoryStore], {
            counter: null, 
            idProperty: "id", 
            put: function(item, options) {
                console.debug("put(", item, ",", options, ")"); 
                if (item.id == 0)
                    throw new Error("can't edit root node"); 
                if (options && options.parent)
                    item.parent = options.parent.id;
                if (!item.id) {
                    if (this.counter == null)
                        this.counter = this.data.length; 
                    item.id = this.counter++; 
                }
                return this.inherited(arguments, [item]); 
            }, 
            remove: function(id) { 
                // Can't remove the root object. 
                if (!id)
                    throw new Error("can't delete root node"); 
                this.query({
                    parent: id
                }).forEach(function(item) {
                    this.remove(item.id); 
                }, this); 
                return this.inherited(arguments); 
            }, 
            getParent: function(item) {
                return this.get(item.parent); 
            }, 
            getChildren: function(item) {
                return this.query({parent: item.id}); 
            }, 
            hasChild: function(item, name) {
                return this.getChild(item, name) != null; 
            }, 
            getChild: function(item, name) {
                if (typeof name != "string")
                    name = name.name; 
                return this.query({
                    name: name, 
                    parent: item.id
                })[0]; 
            }, 
            setData: function(data) { 
                return this.inherited(arguments); 
            }
        }); 
    }
); 