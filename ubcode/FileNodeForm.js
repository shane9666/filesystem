define([
    "dojo/query", 
    "dojo/_base/lang", 
	"dojo/_base/array", 
	"dojo/_base/declare", 
    "dojo/dom-attr", 
	"dojo/dom-construct", 
	"dijit/_WidgetBase", 
	"dijit/form/Button", 
    "dijit/form/Select", 
    "dijit/form/TextBox", 
	"dijit/Dialog", 
 	"dijit/_TemplatedMixin", 
 	"dijit/_WidgetsInTemplateMixin", 
 	// Replace class-name with name of class.  
 	"dojo/text!./templates/FileNodeForm.html"], 
 	function(query, lang, array, declare, domAttr, 
        construct, WidgetBase, Button, Select, TextBox, 
        Dialog, Templated, WidgetsInTemplateMixin, template) {
 		return declare("ubcode.FileNodeForm", 
            [WidgetBase, Templated, WidgetsInTemplateMixin], {
			widgetsInTemplate: true, 
			templateString: template, 
            constructor: function(props, refNode) {
                this._value = {name: "", type: ""}; 
            }, 
			_getValueAttr: function() {
                var value = {}; 
                value.name = this.nameInput.get("value"); 
                value.type = this.typeSelector.get("value"); 
                return value; 
            }, 
            _setValueAttr: function(value) {
                this._value = {
                    name: value.name, 
                    type: value.type
                }; 
                this.nameInput.set("value", value.name); 
                this.typeSelector.set("value", value.type); 
            }, 
            reset: function() {
                this.set("value", this._value); 
            }, 
            onSave: function() {
                var value = this.get("value"); 
                for (var name in value) {
                    if (value[name] == this._value[name])
                        delete(value[name]); 
                }
                // values is hash of changes. 
                this.save(value); 
            }, 
            // Override point: pass in saved as override.
            save: function(changes) {}
		}); // End of declare() return.
	} // End of callback function. 
);
