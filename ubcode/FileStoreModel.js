/********************************
* File system object-store model mediates tree/store interaction.
* 
* Although everything can be achieved without using FS abstraction, 
* providing a file-system specific API makes validation and 
* actions both intuitive and less error prone.
********************************/
define([
    "dojo/Stateful", 
    "dojo/_base/lang", 
    "dojo/_base/declare",  
   	"dojo/_base/array", 
    "dijit/tree/ObjectStoreModel"], 
	function(Stateful, lang, declare, array, ObjectStoreModel) {
        return declare("ubcode.FileStoreModel", [ObjectStoreModel], {
            // Dojo tree API requirement.
            getRoot: function(onItem) {
                var rootNode = this.store.get(0); 
                if (!onItem)
                    return rootNode; 
                return onItem(rootNode); 
            }, 
            asItem: function(spec) {
                if (typeof spec == "string")
                    return this.getItem(spec); 
                if (typeof spec == "object" && typeof(spec.id) == "number")
                    spec = spec.id
                if (typeof spec == "number")
                    return this.store.get(spec); 
                throw new Error("cannot deduce item: " + spec); 
            }, 
            /*************
             * Get filesystem path from object-store item. 
             ************/
            getPath: function(item) {
                var names = []; 
                item = this.asItem(item); 
                while (item) {
                    names.unshift(item.name); 
                    item = this.store.getParent(item); 
                }
                return names.join("/").slice(1) || "/"; 
            }, 
            /*************
             * Get object-store item from filesystem path. 
             ************/
            getItem: function(path, editable) {
                var self = this; 
                var item = this.getRoot(); 
                // Because editable usage callers may invoke with item.
                if (typeof path != "string")
                    path = this.getPath(path); 
                path = path.slice(1); 
                if (path) {
                    var names = path.split("/"); 
                    while (names.length) {
                        item = this.store.query({
                            parent: item.id, 
                            name: names.shift()
                        })[0]; 
                        if (!item)
                            throw new Error("no such path")
                    }
                }
                if (editable) {
                    item = Stateful(item); 
                    item.watch(function(name, oldValue, newValue) {
                        self.store.put(item); 
                    }); 
                }
                return item; 
            }, 
            /******************
             * Check whether provided path exists. 
             *****************/
            exists: function(path) {
                try {
                    this.getItem(path); 
                } catch (error) {
                    return false; 
                }
                return true;
            }, 
            /*******************
             * Make new file node of "file" or "folder" type.
             ******************/
            make: function(path, name, type) {
                var filepath = path + ((path == "/")? "" :"/") + name; 
                if (this.exists(filepath))
                    throw new Error(filepath + " exists"); 
                else if (this.exists(path) && !this.isFolder(path))
                    throw new Error(path + " is not a folder"); 
                var item = this.getRoot(); 
                var names = filepath.split("/").slice(1); 
                while (names.length) {
                    var name = names.shift(); 
                    if (!this.store.hasChild(item, name)) {
                        this.store.put({
                            name: name, 
                            type: (names.length) ? "folder" : type
                        }, {parent: item}); 
                    }
                    item = this.store.query({
                        name: name, 
                        parent: item.id
                    })[0]; 
                }
                return item; 
            }, 
            makeItem: function(values, parent) {
                var id = this.store.put(values, {
                    parent: this.asItem(parent)
                }); 
                return this.store.get(id); 
            }, 
            /*******************
             * Log details about this file/directory at path. 
             ******************/
            log: function(spec) {
                var item = this.asItem(spec); 
                var path = this.getPath(item); 
                var type = (item.type == "folder") ? "D" : "F"; 
                console.log("[" + item.id + "][" + 
                    type + "]\t--\t" + path); 
                if (item.type == "folder") {
                    array.forEach(this.store.getChildren(item), 
                        function(child) {
                            console.log(
                                "  -> '" + child.name + 
                                "' (" + child.type + ")" 
                            ); 
                        }
                    ); 
                }
            }, 
            /*********************
             * Get list of names for contents of path. 
             *********************/
            ls: function(spec) {
                var item = this.asItem(spec); 
                var children = []; 
                if (item.type == "folder")
                    children = this.store.getChildren(item); 
                return array.map(children, function(item) {
                    return item.name; 
                }); 
            }, 
            mkdir: function(path, name) {
                if (typeof path != "string")
                    path = this.getPath(path); 
                if (arguments.length == 1) {
                    var index = path.lastIndexOf("/"); 
                    name = path.slice(index + 1); 
                    path = path.slice(0, index); 
                }
                return this.make(path, name, "folder"); 
            }, 
            touch: function(path, name) {
                if (typeof path != "string")
                    path = this.getPath(path); 
                if (arguments.length == 1) {
                    var index = path.lastIndexOf("/"); 
                    name = path.slice(index + 1); 
                    path = path.slice(0, index); 
                }
                return this.make(path, name, "file"); 
            }, 
            move: function(source, destination) {
                var item = this.asItem(source); 
                var parent = this.asItem(destination); 
                if (!parent.type == "folder")
                    throw new Error("destination not a folder"); 
                else if (this.store.hasChild(parent, item.name))
                    throw new Error("child exists"); 
                return this.store.put(item, {parent: parent}); 
            }, 
            rename: function(source, name) {
                // Original item before move.  
                var item = this.asItem(source); 
                var parent = this.store.getParent(item); 
                if (this.store.hasChild(parent, name))
                    throw new Error("child exists"); 
                return this.update(item, {name: name}); 
            }, 
            isFolder: function(item) {
                if (typeof item == "string")
                    item = this.getItem(item); 
                return item.type == "folder"; 
            }, 
            hasChild: function(spec, name) {
                if (typeof name != "string")
                    name = name.name; 
                return this.store.hasChild(this.asItem(spec), name); 
            }, 
            hasChildren: function(spec) {
                return this.ls(spec).length; 
            }, 
            getValue: function(item, recurse) {
                if (typeof item == "string")
                    item = this.getItem(item); 
                var value =  {
                    name: item.name, 
                    type: item.type
                }; 
                if (recurse && this.isFolder(item)) {
                    var items = this.store.getChildren(item); 
                    value.contents = array.map(items, function(item) {
                        return this.getValue(item, true); 
                    }, this); 
                }
                return value; 
            }, 
            isRoot: function(spec) {
                return this.asItem(spec) == this.getRoot(); 
            }, 
            getContent: function(item) {
                item = item || this.getRoot(); 
                return this.getValue(item || this.getRoot(), true); 
            }, 
            getChildren: function(parentItem, onComplete, onError) {
                console.log(this.declaredClass, "getChildren()", parentItem); 
                var parent = this.asItem(parentItem); 
                if (arguments.length == 1)
                    return this.store.getChildren(parent); 
                return this.inherited(arguments, [parent,onComplete,onError]); 
            }, 
            onChildrenChange: function(parent, newChildrenList) {
                console.log(this.declaredClass, 
                    "onChildrenChange", parent, newChildrenList); 
                return this.inherited(arguments); 
            }, 
            update: function(item, values) { 
                var storeItem = this.asItem(item); 
                if (storeItem != item && lang.isObject(item))
                    lang.mixin(storeItem, item); 
                if (values)
                    lang.mixin(storeItem, values); 
                return this.asItem(this.store.put(storeItem)); 
            }
        });  
    }
);  