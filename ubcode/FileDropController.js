/************************
* Tree controller adds File DnD from dekstop support.
************************/
define([
    "dojo/Stateful", 
    "dojo/_base/array", 
    "dojo/_base/lang", 
    "dojo/_base/declare",  
    "dijit/_WidgetBase", 
    "dijit/registry", 
    "dijit/Dialog", 
    "dijit/_Container", "dojo/on"], 
    function(Stateful, array, lang, declare, 
        WidgetBase, registry, Dialog, Container, on) {
        return declare("ubcode.FileDropController", [Stateful], {
            tree: null, 
            constructor: function(props) {                
                this.connections = []; 
                console.log(this.declaredClass, "constructor()"); 
            }, 
            _treeSetter: function(tree) {
                console.log(this.declaredClass, "set('tree', tree)"); 
                while(this.connections.length)
                    this.connections.shift().remove(); 
                var self = this; 
                this.tree = tree; 
                this.connections.push(on(
                    tree.domNode, "dragenter", function(event) {
                    event.preventDefault(); 
                })); 
                this.connections.push(on(
                    tree.domNode, "dragover", function(event) {
                    event.preventDefault(); 
                })); 
                this.connections.push(on(
                    tree.domNode, "dragleave", function(event) {
                })); 
                this.connections.push(on(
                    tree.domNode, "drop", function(event) {
                    event.preventDefault(); 
                    self.onDropEvent(event); 
                })); 
                this.inherited(arguments); 
            }, 
            onDropEvent: function(event) {
                var transfer = event.dataTransfer; 
                var transferItems = transfer.items || []; 
                var transferFiles = transfer.files || []; 
                var transferFileNames = []; 
                array.forEach(transferFiles, function(item) {
                    transferFileNames.push(item.name); 
                }); 
                var widget = registry.getEnclosingWidget(event.target); 
                this.handleFileDrop(widget, transferFileNames);
            }, 
            handleFileDrop: function(treeItem, fileNames) {
                var failures = []; 
                var item = treeItem.item; 
                var model = this.tree.model; 
                array.forEach(fileNames, function(name) {
                    try {
                        this.tree.model.touch(item, name); 
                    } catch (error) {
                        failures.push(error.toString()); 
                    }
                }, this); 
                if (failures.length) {
                    var header = "<h2>Failures Ocurred</h2>"; 
                    var message = failures.join("<br />\n"); 
                    var warning = new Dialog({
                        content: header + "\n" + message
                    }); 
                    warning.show(); 
                }
            }
        });
    }
); 